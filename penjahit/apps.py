from suit.apps import DjangoSuitConfig
from suit.menu import ParentItem, ChildItem

class SuitConfig(DjangoSuitConfig):
    layout = 'vertical'
    admin_name = 'ashdkjasd'
    menu = (
    	ParentItem('Transaksi', url='admin:transaksi_transaksi_changelist' , icon='fa fa-cog'),
    	ParentItem('Account', children=[
            ChildItem('Akun', url='admin:accounts_account_changelist'),
            ChildItem('Penjahit', url='admin:accounts_penjahit_changelist'),
            ChildItem('Customer', url='admin:accounts_customer_changelist'),
            ChildItem('Hak Akses', url='/auth/group/'),

        ], align_right=True, icon='fa fa-user'),
        ParentItem('Ukuran', url='admin:transaksi_ukuran_changelist' , icon='fa fa-cog'),
        ParentItem('Model Baju', url='admin:transaksi_modelbaju_changelist' , icon='fa fa-cog'),
        ParentItem('Waktu Pengerjaan', url='admin:transaksi_waktupengerjaan_changelist' , icon='fa fa-cog'),
        ParentItem('Biaya Tambahan', url='admin:transaksi_biayatambahan_changelist' , icon='fa fa-dollar'),
        ParentItem('Kuesioner', children=[
            ChildItem('Pertanyaan', url='admin:transaksi_pertanyaankuesioner_changelist'),
            ChildItem('Kuesioner', url='admin:transaksi_kuesioner_changelist'),
        ], align_right=True, icon='fa fa-quote-right'),

        ParentItem('Master', children=[
            ChildItem('Kategori Waktu Pengerjaan', url='admin:accounts_kategoriwaktupengerjaan_changelist'),
            ChildItem('Kategori Harga', url='admin:accounts_kategoriharga_changelist'),
            ChildItem('Kategori Penjahit', url='admin:accounts_kategoripenjahit_changelist'),

        ], align_right=True, icon='fa fa-user'),
        
    )