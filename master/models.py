from django.db import models
from datetime import datetime
from .utils import STATUS, get_status_color

class AtributTambahan(models.Model):
	status = models.PositiveSmallIntegerField(verbose_name='Status Data', choices=STATUS, default=6)
	# status = models.CharField(verbose_name="Status Data", choices=STATUS, default=1, max_length=20)
	created_by = models.ForeignKey("accounts.Account", related_name="%(app_label)s_%(class)s_create_by_user", verbose_name="Dibuat Oleh", blank=True, null=True, on_delete=models.SET_NULL)
	created_at = models.DateTimeField(auto_now_add=True, null=True)
	verified_by = models.ForeignKey("accounts.Account", related_name="%(app_label)s_%(class)s_verify_by_user", verbose_name="Diverifikasi Oleh", blank=True, null=True, on_delete=models.SET_NULL)
	verified_at = models.DateTimeField(editable=False, blank=True, null=True)
	rejected_by = models.ForeignKey("accounts.Account", related_name="%(app_label)s_%(class)s_rejected_by_user", verbose_name="Dibatalkan Oleh", blank=True, null=True, on_delete=models.SET_NULL)
	rejected_at = models.DateTimeField(editable=False, blank=True, null=True)
	updated_at = models.DateTimeField(auto_now=True)

	# def get_color_status(self):
	# 	return get_status_color(self)
		
	def save(self, *args, **kwargs):
		''' On save, update timestamps '''
		if not self.id:
			print("askjdhakjsd")
			self.created_at = datetime.now()
		self.updated_at = datetime.now()
		return super(AtributTambahan, self).save(*args, **kwargs)

	def __unicode__(self):
		return u'%s' % (str(self.status))

	class Meta:
		abstract = True