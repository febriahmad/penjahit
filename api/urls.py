# from django.contrib import admin
# from rest_framework import routers
from django.urls import path, include
from django.conf.urls import url
from rest_framework.authtoken.views import obtain_auth_token
from api import api as NeiraAPI
from .views import home, menu
# router = routers.DefaultRouter()

urlpatterns = [
	# url(r'^$', home, name="home_page"),
	url(r'^menu-pengguna$', menu, name="get_menu_pengguna"),

	path('api-token-auth/', obtain_auth_token, name='api-token-auth'),

	url(r'^api/accounts/kategoriwaktupengerjaan/list/$', NeiraAPI.AccountsAPI.kategoriwaktupengerjaan_list),
	url(r'^api/accounts/kategoriharga/list/$', NeiraAPI.AccountsAPI.kategoriharga_list),
	url(r'^api/accounts/kategoripenjahit/list/$', NeiraAPI.AccountsAPI.kategoripenjahit_list),

	url(r'^api/accounts/account/get-profil/$', NeiraAPI.ProfileAPI.get_profile),
	url(r'^api/accounts/account/get-user-akses/$', NeiraAPI.ProfileAPI.get_user_akses),
	url(r'^api/accounts/account/logout/$', NeiraAPI.ProfileAPI.logout),
	url(r'^api/accounts/customer/update/$', NeiraAPI.ProfileAPI.update_profil_customer),
	url(r'^api/accounts/customer/daftar/$', NeiraAPI.CustomerAPI.daftar_akun),
	url(r'^api/accounts/penjahit/list/$', NeiraAPI.PenjahitAPI.list_penjahit),
	url(r'^api/accounts/penjahit/customer/list/$', NeiraAPI.PenjahitAPI.list_penjahit_customer),
	url(r'^api/accounts/penjahit/daftar/$', NeiraAPI.PenjahitAPI.daftar_akun),
	url(r'^api/accounts/penjahit/data/$', NeiraAPI.PenjahitAPI.get_penjahit_profil),
	url(r'^api/accounts/penjahit/update/$', NeiraAPI.PenjahitAPI.update_profil),
	
	url(r'^api/transaksi/riwayattransaksi/list/$', NeiraAPI.TransaksiAPI.list_riwayat),
	url(r'^api/transaksi/transaksi/list/$', NeiraAPI.TransaksiAPI.list_transaksi),
	url(r'^api/transaksi/transaksi/save/$', NeiraAPI.TransaksiAPI.simpan_transaksi),
	url(r'^api/transaksi/transaksi/verifikasi/$', NeiraAPI.TransaksiAPI.verifikasi_transaksi),
	url(r'^api/transaksi/transaksi/upload-bukti/$', NeiraAPI.TransaksiAPI.aksi_upload_bukti_transfer),
	url(r'^api/transaksi/transaksi/check/$', NeiraAPI.TransaksiAPI.check_transaksi_status),
	url(r'^api/transaksi/ukuran/list/$', NeiraAPI.TransaksiAPI.list_ukuran),
	url(r'^api/transaksi/model-baju/list/$', NeiraAPI.TransaksiAPI.list_model_baju),
	url(r'^api/transaksi/model-baju/add/$', NeiraAPI.TransaksiAPI.simpan_tambah_produk),
	url(r'^api/transaksi/model-baju/edit/$', NeiraAPI.TransaksiAPI.simpan_edit_produk),
	url(r'^api/transaksi/waktu-pengerjaan/list/$', NeiraAPI.TransaksiAPI.list_waktu_pengerjaan),
	url(r'^api/transaksi/biaya-tambahan/list/$', NeiraAPI.TransaksiAPI.list_biaya_tambahan),
	url(r'^api/transaksi/biaya-tambahan/trigger-total/$', NeiraAPI.TransaksiAPI.trigger_total_harga),
	url(r'^api/transaksi/kuesioner/pertanyaan/list/$', NeiraAPI.TransaksiAPI.list_pertanyaan_kuesioner),
	url(r'^api/transaksi/kuesioner/jawaban/save/$', NeiraAPI.TransaksiAPI.save_jawaban_kuesioner),
	url(r'^api/transaksi/kuesioner/jawaban/save/reload/$', NeiraAPI.TransaksiAPI.save_pesan_kuesioner_and_reload_rating),
]