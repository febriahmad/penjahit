from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.response import Response

from django.utils.safestring import mark_safe

from accounts.models import Penjahit, Customer, KategoriWaktuPengerjaan, KategoriHarga, KategoriPenjahit
from transaksi.models import Ukuran, ModelBaju, WaktuPengerjaan, Transaksi, BiayaTambahan, RiwayatTransaksi, PertanyaanKuesioner, Kuesioner

from master.utils import formatrupiah

class AccountsAPI():
	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def kategoriwaktupengerjaan_list(request):
		respon = {'success': False}
		list_data = KategoriWaktuPengerjaan.objects.all()
		if list_data:
			respon = {'success': True, 'data': [ob.as_json() for ob in list_data]}
		return Response(respon)

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def kategoriharga_list(request):
		respon = {'success': False}
		list_data = KategoriHarga.objects.all()
		if list_data:
			respon = {'success': True, 'data': [ob.as_json() for ob in list_data]}
		return Response(respon)

	@api_view(['GET'])
	@authentication_classes([])
	@permission_classes([])
	def kategoripenjahit_list(request):
		respon = {'success': False}
		list_data = KategoriPenjahit.objects.all()
		if list_data:
			respon = {'success': True, 'data': [ob.as_json() for ob in list_data]}
		return Response(respon)

class CustomerAPI():
	@api_view(['POST'])
	@authentication_classes([])
	@permission_classes([])
	def daftar_akun(request):
		respon = {'success': False}
		# print(request.POST)
		nama_lengkap = request.POST.get("nama_lengkap", None)
		telephone = request.POST.get('telephone', None)
		email = request.POST.get('email', None)
		password = request.POST.get('password', None)
		alamat = request.POST.get('alamat')

		if email:
			if telephone:
				if password:
					try:
						customer_obj = Customer.objects.get(email=email)
						respon = {'success': False, 'pesan': 'email yang Anda masukan telah terdaftar di sistem. Mohon gunakan email yang lainnya.'}
						return Response(respon)
					except Customer.DoesNotExist:
						username = email.split('@')[0]
						customer_list = Customer.objects.filter(username=username)
						if customer_list.exists():
							urutan = customer_list.count()+1
							# print(customer_list)
							# print(urutan)
							username = username+str(urutan)
						try:
							customer_obj = Customer.objects.get(telephone=telephone)
							respon = {'success': False, 'pesan': 'Nomor telephone telah terdaftar di sistem. Mohon gunakan nomor telephone yang lainnya.'}
							return Response(respon)
						except Customer.DoesNotExist:
							# create user
							# print(username)
							try:
								customer_obj = Customer(
									username=username,
									email=email,
									nama_lengkap=nama_lengkap,
									telephone=telephone,
									alamat=alamat
									)
								customer_obj.save()
							except Exception:
								import random
								username = username+str(random.randint(1, 999))
								customer_obj = Customer(
									username=username,
									email=email,
									nama_lengkap=nama_lengkap,
									telephone=telephone,
									alamat=alamat
									)
								customer_obj.save()
							customer_obj.set_password(password)
							customer_obj.save()
							if customer_obj:
								from rest_framework.authtoken.models import Token
								token = Token.objects.create(user=customer_obj)
								respon = {'success': True, 'pesan': 'User berhasil terdaftar.', 'token': token.key}
							else:
								respon = {'success': False, 'pesan': 'terjadi kesalahan saat proses simpan'}
							return Response(respon)
				else:
					respon = {'success': False, 'pesan': 'Password tidak boleh kosong.'}
			else:
				respon = {'success': False, 'pesan': 'Telephone tidak boleh kosong.'}
		else:
			respon = {'success': False, 'pesan': 'Email tidak boleh kosong.'}
		return Response(respon)


class ProfileAPI():
	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def get_profile(request):
		respon = {'success': False}
		if request.user:
			respon = {'success': True, 'data': request.user.as_json()}
		return Response(respon)

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def logout(request):
		respon = {'success': False}
		if request.user:
			request.user.auth_token.delete()
			respon = {'success': True}
		return Response(respon)

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def get_user_akses(request):
		respon = {'success': True}
		print(request.user.is_penjahit())
		print(request.user.is_customer())

		if request.user.is_penjahit():
			respon = {'success': True, 'hak_akses': 'penjahit', 'nama_lengkap': request.user.nama_lengkap}
		elif request.user.is_customer():
			respon = {'success': True, 'hak_akses': 'customer', 'nama_lengkap': request.user.nama_lengkap}
		else:
			respon = {'success': False, 'hak_akses': ''}
		return Response(respon)

	@api_view(['POST'])
	@permission_classes((IsAuthenticated,))
	def update_profil_customer(request):
		respon = {'success': False, 'pesan': 'Data tidak ditemukan'}
		if request.user.is_customer():
			customer = request.user.customer
			email = request.POST.get('email', None)
			nama_penjahit = request.POST.get('nama_lengkap', None)
			alamat = request.POST.get('alamat', None)
			telephone = request.POST.get('telephone', None)

			if email:
				customer.email = email
			customer.nama_lengkap = nama_penjahit
			customer.alamat = alamat
			customer.telephone = telephone
			if request.POST.get('foto', None):
				from django.core.files.base import ContentFile
				import base64, uuid, imghdr, os
				try:
					decoded_file = base64.b64decode(request.POST.get('foto', None))
				except TypeError:
					respon = {'success': False, 'pesan': 'Terjadi kesalahan, file yang diupload bukan file gambar'}
					return Response(respon)
				format, imgstr = request.POST.get("foto").split(';base64,') 
				ext = format.split('/')[-1]
				foto = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
				customer.foto = foto
			customer.save()
			respon = {'success': True, 'pesan': 'Berhasil update'}
		return Response(respon)

class PenjahitAPI():
	@api_view(['POST'])
	@authentication_classes([])
	@permission_classes([])
	def daftar_akun(request):
		respon = {'success': True}
		# print(request.POST)
		nama_lengkap = request.POST.get("nama_lengkap", None)
		telephone = request.POST.get('telephone', None)
		email = request.POST.get('email', None)
		password = request.POST.get('password', None)
		alamat = request.POST.get('alamat', None)
		nomor_rekening = request.POST.get('nomor_rekening', None)
		lt = request.POST.get('lt', None)
		lg = request.POST.get('lg', None)
		kategoripenjahit_list = request.POST.getlist('kategoripenjahit[]', None)

		if email:
			if telephone:
				if password:
					try:
						customer_obj = Penjahit.objects.get(email=email)
						respon = {'success': False, 'pesan': 'email yang Anda masukan telah terdaftar di sistem. Mohon gunakan email yang lainnya.'}
						return Response(respon)
					except Penjahit.DoesNotExist:
						username = email.split('@')[0]
						customer_list = Penjahit.objects.filter(username=username)
						if customer_list.exists():
							urutan = customer_list.count()+1
							# print(customer_list)
							# print(urutan)
							username = username+str(urutan)
						try:
							customer_obj = Penjahit.objects.get(telephone=telephone)
							respon = {'success': False, 'pesan': 'Nomor telephone telah terdaftar di sistem. Mohon gunakan nomor telephone yang lainnya.'}
							return Response(respon)
						except Penjahit.DoesNotExist:
							# create user
							# print(username)
							try:
								customer_obj = Penjahit(
									username=username,
									email=email,
									nama_lengkap=nama_lengkap,
									telephone=telephone,
									alamat=alamat,
									no_rekening=nomor_rekening,
									lt=lt,
									lg=lg
									)
								customer_obj.save()
							except Exception:
								import random
								username = username+str(random.randint(1, 999))
								customer_obj = Penjahit(
									username=username,
									email=email,
									nama_lengkap=nama_lengkap,
									telephone=telephone,
									alamat=alamat,
									no_rekening=nomor_rekening,
									lt=lt,
									lg=lg
									)
								customer_obj.save()
							customer_obj.is_staff = True
							customer_obj.set_password(password)
							customer_obj.save()

							if customer_obj:
								if kategoripenjahit_list:
									_len_ = len(kategoripenjahit_list)
									for k in range(_len_):
										if kategoripenjahit_list[k] != "":
											customer_obj.kategori_penjahit.add(kategoripenjahit_list[k])

								from django.contrib.auth.models import Group
								try:
									my_group = Group.objects.get(name='penjahit')
								except Group.DoesNotExist:
									pass
								else:
									my_group.user_set.add(customer_obj)
									
								from rest_framework.authtoken.models import Token
								token = Token.objects.create(user=customer_obj)
								respon = {'success': True, 'pesan': 'User berhasil terdaftar.', 'token': token.key}
							else:
								respon = {'success': False, 'pesan': 'terjadi kesalahan saat proses simpan'}
							return Response(respon)
				else:
					respon = {'success': False, 'pesan': 'Password tidak boleh kosong.'}
			else:
				respon = {'success': False, 'pesan': 'Telephone tidak boleh kosong.'}
		else:
			respon = {'success': False, 'pesan': 'Email tidak boleh kosong.'}
		return Response(respon)

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def list_penjahit_customer(request):
		from geopy.distance import geodesic
		respon = {'success': False, 'pesan': 'Data tidak ditemukan'}
		q = request.GET.get('q', None)
		mylokasi_lat = request.GET.get('mylokasi_lat', None)
		mylokasi_long = request.GET.get('mylokasi_long', None)
		filter_ = request.GET.get('filter', None)
		if q:
			penjahit_list = Penjahit.objects.filter(nama_lengkap__icontains=q)
		else:
			penjahit_list = Penjahit.objects.all()

		penjahit_acuan_data_set = None
		try:
			penjahit_obj_acuan = Penjahit.objects.get(jadi_acuan=True)
		# penjahit_obj_acuan = Penjahit.objects.last()
		except Penjahit.DoesNotExist:
			pass
		else:
		# if penjahit_obj_acuan:
			# print("Data Acuan : "+penjahit_obj_acuan.username)
			if penjahit_obj_acuan.kategori_waktu_pengerjaan and penjahit_obj_acuan.kategori_harga and penjahit_obj_acuan.rating:
				data_set_waktu = penjahit_obj_acuan.kategori_waktu_pengerjaan.id
				data_set_harga = penjahit_obj_acuan.kategori_harga.id
				data_set_rating = penjahit_obj_acuan.get_rating_hitung()
				penjahit_acuan_data_set = {'data_set_waktu': data_set_waktu, 'data_set_harga': data_set_harga, 'data_set_rating': data_set_rating}

			penjahit_list = penjahit_list.exclude(id=penjahit_obj_acuan.id)

		list_data = []
		if penjahit_list.exists():
			for p in penjahit_list:
				jarak = 0
				nilai_distance = 0
				nilai_kemiripan = 0
				if mylokasi_lat and mylokasi_long and p.lt and p.lg:

					my_lokasi = (mylokasi_lat+", "+mylokasi_long)
					lokasi_penjahit = (p.lt+", "+p.lg)

					km = geodesic(my_lokasi, lokasi_penjahit).kilometers

					jarak = round(km, 1)

				if p.kategori_waktu_pengerjaan and p.kategori_harga and p.rating:
					data_set_waktu = p.kategori_waktu_pengerjaan.id
					data_set_harga = p.kategori_harga.id
					data_set_rating = p.get_rating_hitung()

					# print("Waktu: "+str(data_set_waktu)+" | Harga: "+str(data_set_harga)+" | Rating:"+str(data_set_rating))

					if penjahit_acuan_data_set:
						hasil_harga = (penjahit_acuan_data_set['data_set_harga']-data_set_harga)
						hasil_harga = hasil_harga*hasil_harga
						hasil_waktu = (penjahit_acuan_data_set['data_set_waktu']-data_set_waktu)
						hasil_waktu = hasil_waktu*hasil_waktu
						hasil_rating = (penjahit_acuan_data_set['data_set_rating']-data_set_rating)
						hasil_rating = hasil_rating*hasil_rating
						nilai_distance = hasil_harga+hasil_waktu+hasil_rating
						# print("Nilai Distance : "+str(nilai_distance))

						nilai_kemiripan = 1/1+nilai_distance
						# print("Nilai Kemiripan : "+str(nilai_kemiripan))

				# print(jarak)
				# rating__ = p.get_rating_hitung()
				# print(p.username+" : "+str(rating__))

				data_ = {'nama_lengkap': p.nama_lengkap, 'jarak': jarak, 'rating': p.rating, 'id': p.id, 'alamat': p.alamat, 'foto': p.get_foto(), 'harga': p.harga, 'waktupengerjaan': p.waktu_pengerjaan, 'nilai_distance': nilai_distance, 'nilai_kemiripan': nilai_kemiripan}
				list_data.append(data_)
			list_filter = ['jarak', 'rating', 'waktupengerjaan', 'harga']
			list_data.sort(key=lambda x: x.get('nilai_kemiripan'))
			if filter_:
				if filter_ == 'jarak':
					list_data.sort(key=lambda x: x.get('jarak'))
			# 	if filter_ == 'rating':
			# 		list_data.sort(key=lambda x: x.get(filter_), reverse=True)
			# 	else:
			# 		list_data.sort(key=lambda x: x.get(filter_))
			# else:
			# 	list_data.sort(key=lambda x: x.get('jarak'))

			# hapus data acuan dari list
			# list_data.remove(x:)

			
			respon = {'success': True, 'jumlah': penjahit_list.count() ,'data': list_data}
		return Response(respon)

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def list_penjahit(request):
		respon = {'success': False, 'pesan': 'Data tidak ditemukan'}
		penjahit_id = request.GET.get('id', None)
		q = request.GET.get('q', None)
		if penjahit_id:
			try:
				penjahit_obj = Penjahit.objects.get(id=penjahit_id)
			except Penjahit.DoesNotExist:
				pass
			else:
				respon = {'success': True, 'data': penjahit_obj.as_json()}
				return Response(respon)
		if q:
			penjahit_list = Penjahit.objects.filter(nama_lengkap__icontains=q)
		else:
			penjahit_list = Penjahit.objects.all()
		if penjahit_list.exists():
			respon = {'success': True, 'data': [ob.as_json() for ob in penjahit_list]}
		return Response(respon)

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def get_penjahit_profil(request):
		respon = {'success': False, 'pesan': 'Data tidak ditemukan'}
		if request.user.is_penjahit():
			penjahit = request.user.penjahit
			respon = {'success': True, 'data': penjahit.as_json()}
		return Response(respon)

	@api_view(['POST'])
	@permission_classes((IsAuthenticated,))
	def update_profil(request):
		respon = {'success': False, 'pesan': 'Data tidak ditemukan'}
		if request.user.is_penjahit():
			penjahit = request.user.penjahit
			aksi = request.POST.get('aksi', None)
			if aksi:
				if aksi == "uncheck":
					kategori_penjahit_id = request.POST.get('kategori_penjahit_id', None)
					penjahit.kategori_penjahit.remove(kategori_penjahit_id)
					respon = {'success': True, 'pesan': 'Berhasil Uncheck Kategori Penjahit'}
					return Response(respon)

			email = request.POST.get('email', None)
			nama_penjahit = request.POST.get('nama_penjahit', None)
			alamat = request.POST.get('alamat', None)
			telephone = request.POST.get('telephone', None)
			no_rekening = request.POST.get('no_rekening', None)
			lt = request.POST.get('lt', None)
			lg = request.POST.get('lg', None)
			waktu_pengerjaan = request.POST.get('waktu_pengerjaan', None)
			kategoriwaktupengerjaan_id = request.POST.get('kategoriwaktupengerjaan_id', None)
			harga = request.POST.get('harga', None)
			kategoriharga_id = request.POST.get('kategoriharga_id', None)
			kategoripenjahit_list = request.POST.getlist('kategoripenjahit[]', None)

			if email:
				penjahit.email = email
			penjahit.nama_lengkap = nama_penjahit
			penjahit.alamat = alamat
			penjahit.telephone = telephone
			penjahit.no_rekening = no_rekening
			penjahit.lt = lt
			penjahit.lg = lg
			penjahit.waktu_pengerjaan = waktu_pengerjaan
			penjahit.kategori_waktu_pengerjaan_id = kategoriwaktupengerjaan_id
			penjahit.harga = harga
			penjahit.kategori_harga_id = kategoriharga_id

			if kategoripenjahit_list:
				_len_ = len(kategoripenjahit_list)
				for k in range(_len_):
					if kategoripenjahit_list[k] != "":
						penjahit.kategori_penjahit.add(kategoripenjahit_list[k])
						
			if request.POST.get('foto', None):
				from django.core.files.base import ContentFile
				import base64, uuid, imghdr, os
				try:
					decoded_file = base64.b64decode(request.POST.get('foto', None))
				except TypeError:
					respon = {'success': False, 'pesan': 'Terjadi kesalahan, file yang diupload bukan file gambar'}
					return Response(respon)
				format, imgstr = request.POST.get("foto").split(';base64,') 
				ext = format.split('/')[-1]
				foto = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
				penjahit.foto = foto
			penjahit.save()
			respon = {'success': True, 'pesan': 'Berhasil update'}
		return Response(respon)

class TransaksiAPI():
	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def save_jawaban_kuesioner(request):
		respon = {'success': False}
		transaksi_id = request.GET.get('transaksi_id', None)
		pertanyaan_id = request.GET.get('pertanyaan_id', None)
		jawaban = request.GET.get('jawaban', None)
		jawaban_ = False
		if jawaban:
			if jawaban == "Yes":
				jawaban_ = True
			elif jawaban == "No":
				jawaban_ = False
		if transaksi_id:
			try:
				transaksi_obj = Transaksi.objects.get(id=transaksi_id)
			except Transaksi.DoesNotExist:
				pass
			else:
				try:
					pertanyaan_obj = PertanyaanKuesioner.objects.get(id=pertanyaan_id)
				except PertanyaanKuesioner.DoesNotExist:
					pass
				else:
					kuesioner_list = Kuesioner.objects.filter(transaksi=transaksi_obj, pertanyaan=pertanyaan_obj)
					if kuesioner_list.exists():
						# Update jawaban
						print("update")
						kuesioner_obj = kuesioner_list.last()
						kuesioner_obj.jawaban = jawaban_
					else:
						# Create jawaban
						print("create")
						kuesioner_obj = Kuesioner(
							transaksi=transaksi_obj,
							pertanyaan=pertanyaan_obj,
							jawaban=jawaban_
							)
					kuesioner_obj.save()

					# print(pertanyaan_obj)
					# print(transaksi_obj)
					# print(jawaban_)
					
					respon = {'success': True, 'pesan': 'Berhasil simpan jawaban'}
		return Response(respon)

	@api_view(['POST'])
	@permission_classes((IsAuthenticated,))
	def save_pesan_kuesioner_and_reload_rating(request):
		respon = {'success': False}
		transaksi_id = request.POST.get('transaksi_id', None)
		keterangan = request.POST.get('keterangan', None)
		if transaksi_id:
			print(transaksi_id)
			try:
				transaksi_obj = Transaksi.objects.get(id=transaksi_id)
			except Transaksi.DoesNotExist:
				pass
			else:
				print(transaksi_obj.status)
				transaksi_obj.keterangan_rating = keterangan
				transaksi_obj.status = 12
				transaksi_obj.save()
				# print(transaksi_obj.keterangan_rating)
				riwayat = RiwayatTransaksi(
					transaksi=transaksi_obj,
					nama_riwayat='Transaksi selesai.'
					)
				riwayat.save()

				'''
					update rating toko hitung ulang nilai kuesioner
				'''
				if transaksi_obj.penjahit:
					kuesioner_list = transaksi_obj.kuesioner_set.all()
					pertanyaan_list = PertanyaanKuesioner.objects.all()
					if kuesioner_list.exists():
						print(kuesioner_list)
						if kuesioner_list.count() > 0:
							print(kuesioner_list.count())
							print(pertanyaan_list.count())
							if kuesioner_list.count() == pertanyaan_list.count():
								print('masuk sini')
								# kuesioner_penjahit_list = Kuesioner.objects.filter(transaksi__penjahit=transaksi_obj.penjahit)
								# for k in kuesioner_penjahit_list:
								transaksi_kuesioner_penjahit_list = Transaksi.objects.filter(penjahit=transaksi_obj.penjahit, status=12)
								
								if transaksi_kuesioner_penjahit_list.exists():
									print(transaksi_kuesioner_penjahit_list)
									jumlah_nilai = 0
									for tkp in transaksi_kuesioner_penjahit_list:
										angka = 0

										for kt in tkp.kuesioner_set.all():
											print(kt)
											if kt.jawaban and kt.jawaban == True:
												angka = angka+5

										nilai = angka/pertanyaan_list.count()
										print("nilai")
										print(nilai)
										print(angka)

										jumlah_nilai = jumlah_nilai+nilai
									print(jumlah_nilai)
									hasil_rating = jumlah_nilai/transaksi_kuesioner_penjahit_list.count()
									print("hasil akhir")
									print(hasil_rating)

									transaksi_obj.penjahit.rating = hasil_rating
									transaksi_obj.penjahit.save()
									respon = {'success': True, 'pesan': 'Berhasil'}
							else:
								respon = {'success': False, 'pesan': 'Ada pertanyaan yang belum terjawab'}
						else:
							respon = {'success': False, 'pesan': 'Ada pertanyaan yang belum terjawab'}
					else:
						respon = {'success': False, 'pesan': 'Ada pertanyaan yang belum terjawab'}
				# 		transaksi_list = Transaksi.objects.filter(status=12)
				# 		for k in kuesioner_list:
				# 			for p in pertanyaan_list:
				# 				print()

		return Response(respon)

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def list_pertanyaan_kuesioner(request):
		respon = {'success': False}
		list_data = PertanyaanKuesioner.objects.all()
		if list_data.exists():
			respon = {'success': True, 'data': [ob.as_json() for ob in list_data]}
		return Response(respon)

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def check_transaksi_status(request):
		respon = {'success': False}
		if request.user.is_customer():
			transaksi_list = request.user.customer.customer_transaksi.all()
			if transaksi_list.exists():
				transaksi_list = transaksi_list.filter(status=11)
				if transaksi_list.exists():
					transaksi_obj = transaksi_list.last()
					respon = {'success': True, 'pesan': 'Data ada', 'transaksi_id': transaksi_obj.id}
		return Response(respon)

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def list_riwayat(request):
		respon = {'success': False}
		transaksi_id = request.GET.get('id', None)
		if transaksi_id:
			try:
				transaksi_obj = Transaksi.objects.get(id=transaksi_id)
			except:
				pass
			else:
				riwayat_list = transaksi_obj.riwayattransaksi_set.all()
				respon = {'success': True, 'data': [ob.as_json() for ob in riwayat_list]}
		return Response(respon)

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def verifikasi_transaksi(request):
		respon = {'success': False}
		act = request.GET.get('act', None)
		transaksi_id = request.GET.get('id', None)
		if act and transaksi_id:
			try:
				transaksi_obj = Transaksi.objects.get(id=transaksi_id)
			except Transaksi.DoesNotExist:
				pass
			else:
				if act == 'terimapesanan':
					transaksi_obj.status = 8
					riwayat = RiwayatTransaksi(
						transaksi=transaksi_obj,
						nama_riwayat='Pesanan telah diterima oleh Penjahit'
						)
					riwayat.save()
				elif act == 'pembayaran':
					transaksi_obj.status = 9
					riwayat = RiwayatTransaksi(
						transaksi=transaksi_obj,
						nama_riwayat='Customer telah melakukan pembayaran'
						)
					riwayat.save()
				elif act == 'selesaidijahit':
					transaksi_obj.status = 10
					riwayat = RiwayatTransaksi(
						transaksi=transaksi_obj,
						nama_riwayat='Pesanan telah selesai dijahit'
						)
					riwayat.save()
				elif act == 'dikirim':
					transaksi_obj.status = 11
					riwayat = RiwayatTransaksi(
						transaksi=transaksi_obj,
						nama_riwayat='Pesanan telah dikirim ke Customer'
						)
					riwayat.save()
				elif act == 'selesai':
					transaksi_obj.status = 12
					riwayat = RiwayatTransaksi(
						transaksi=transaksi_obj,
						nama_riwayat='Transaksi selesai'
						)
					riwayat.save()

				elif act == 'tolak':
					transaksi_obj.status = 13
					riwayat = RiwayatTransaksi(
						transaksi=transaksi_obj,
						nama_riwayat='Transaksi telah ditolak oleh penjahit'
						)
					riwayat.save()
				transaksi_obj.save()
				respon = {'success': True, 'pesan': 'berhasil'}
		return Response(respon)

	@api_view(['POST'])
	@permission_classes((IsAuthenticated,))
	def aksi_upload_bukti_transfer(request):
		respon = {'success': False}
		transaksi_id = request.POST.get('id', None)
		if transaksi_id:
			try:
				transaksi_obj = Transaksi.objects.get(id=transaksi_id)
			except Transaksi.DoesNotExist:
				pass
			else:
				transaksi_obj.status = 9
				if request.POST.get('foto', None):
					from django.core.files.base import ContentFile
					import base64, uuid, imghdr, os
					try:
						decoded_file = base64.b64decode(request.POST.get('foto', None))
					except TypeError:
						respon = {'success': False, 'pesan': 'Terjadi kesalahan, file yang diupload bukan file gambar'}
						return Response(respon)
					format, imgstr = request.POST.get("foto").split(';base64,') 
					ext = format.split('/')[-1]
					foto = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
					transaksi_obj.bukti_transfer = foto
					# produk_obj.save()


				riwayat = RiwayatTransaksi(
					transaksi=transaksi_obj,
					nama_riwayat='Customer telah melakukan pembayaran'
					)
				riwayat.save()
				transaksi_obj.save()
				respon = {'success': True, 'pesan': 'Berhasil'}
		return Response(respon)

	@api_view(['POST'])
	@permission_classes((IsAuthenticated,))
	def simpan_transaksi(request):
		respon = {'success': False}
		penjahit_id = request.POST.get('penjahit_id', None)
		produk_id = request.POST.get('produk_id', None)
		biaya_tambahan_id = request.POST.get('biaya_tambahan_id', None)
		ukuran_id = request.POST.get('ukuran_id', None)
		total_harga = request.POST.get('total_harga', None)
		keterangan = request.POST.get('keterangan', None)

		if request.user.is_customer():
			transaksi_obj = Transaksi(
				customer=request.user.customer,
				penjahit_id=penjahit_id,
				baju_id=produk_id,
				ukuran_id=ukuran_id,
				total_harga=total_harga,
				biaya_tambahan_id=biaya_tambahan_id,
				keterangan=keterangan
				)
			transaksi_obj.save()
			riwayat = RiwayatTransaksi(
						transaksi=transaksi_obj,
						nama_riwayat='Pesanan dibuat'
						)
			riwayat.save()

			respon = {'success': True, 'transaksi_id': transaksi_obj.id}
		return Response(respon)

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def trigger_total_harga(request):
		respon = {'success': False}
		produk_id = request.GET.get('produk_id', None)
		biaya_tambahan_id = request.GET.get('biaya_tambahan_id', None)
		if produk_id and biaya_tambahan_id:
			total_harga = 0
			try:
				produk_obj = ModelBaju.objects.get(id=produk_id)
			except ModelBaju.DoesNotExist:
				pass
			else:
				total_harga = total_harga+produk_obj.harga

			try:
				biaya_obj = BiayaTambahan.objects.get(id=biaya_tambahan_id)
			except BiayaTambahan.DoesNotExist:
				pass
			else:
				total_harga = total_harga+biaya_obj.nominal

			respon = {'success': True, 'total_harga': total_harga, 'total_harga_rp': formatrupiah(total_harga, True)}

		return Response(respon)

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def list_ukuran(request):
		respon = {'success': False}
		act = request.GET.get('act', None)
		model_baju_id = request.GET.get('model_baju_id', None)
		ukuran_list = Ukuran.objects.all()
		if act == 'json':
			respon = {'success': True, 'data':  [ob.as_json() for ob in ukuran_list]}
			return Response(respon)

		if model_baju_id:
			try:
				model_baju_obj = ModelBaju.objects.get(id=model_baju_id)
			except ModelBaju.DoesNotExist:
				pass
			else:
				ukuran_list = model_baju_obj.ukuran.all()
		option = '<option value>Pilih Ukuran</option>'
		return Response(mark_safe(option+"".join(x.as_option() for x in ukuran_list)))

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def list_model_baju(request):
		respon = {'success': False}
		penjahit_id = request.GET.get('id', None)
		q = request.GET.get('q', None)
		model_baju_list = None

		model_baju_id = request.GET.get('model_baju_id', None)
		if model_baju_id:
			try:
				model_baju_obj = ModelBaju.objects.get(id=model_baju_id)
			except ModelBaju.DoesNotExist:
				pass
			else:
				print(model_baju_obj)
				respon = {'success': True, 'data': model_baju_obj.as_json()}
				return Response(respon)

		if penjahit_id:
			model_baju_list = ModelBaju.objects.filter(penjahit_id=penjahit_id)
		elif request.user.is_penjahit():
			model_baju_list = ModelBaju.objects.filter(penjahit_id=request.user.id)
		if q:
			model_baju_list = model_baju_list.filter(nama_model_baju__contains=q)
		if model_baju_list:
			respon = {'success': True, 'data': [ob.as_json() for ob in model_baju_list]}
		return Response(respon)

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def list_waktu_pengerjaan(request):
		respon = {'success': False}
		waktu_pengerjaan_list = WaktuPengerjaan.objects.all()
		option = '<option value>Pilih Waktu Pengerjaan</option>'
		return Response(mark_safe(option+"".join(x.as_option() for x in waktu_pengerjaan_list)))

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def list_biaya_tambahan(request):
		respon = {'success': False}
		penjahit_id = request.GET.get('penjahit_id', None)
		if penjahit_id:
			list_data = BiayaTambahan.objects.filter(penjahit_id=penjahit_id)
		else:
			list_data = BiayaTambahan.objects.all()
		option = '<option value>Pilih Waktu Pengerjaan</option>'
		return Response(mark_safe(option+"".join(x.as_option() for x in list_data)))

	@api_view(['GET'])
	@permission_classes((IsAuthenticated,))
	def list_transaksi(request):
		respon = {'success': False, 'pesan': 'Transaksi kosong'}
		transaksi_id = request.GET.get('id', None)
		q = request.GET.get('q', None)
		if transaksi_id:
			try:
				transaksi_obj = Transaksi.objects.get(id=transaksi_id)
			except Transaksi.DoesNotExist:
				pass
			else:
				respon = {'success': True, 'data': transaksi_obj.as_json()}
				return Response(respon)
		if request.user.is_customer():
			transaksi_list = Transaksi.objects.filter(customer=request.user.customer)
		elif request.user.is_penjahit():
			transaksi_list = Transaksi.objects.filter(penjahit=request.user.penjahit)
		if q:
			print(q)
			transaksi_list = transaksi_list.filter(nomor_transaksi__contains=q)
		if transaksi_list.exists():
			respon = {'success': True, 'data': [ob.as_json() for ob in transaksi_list]}
		return Response(respon)

	@api_view(['POST'])
	@permission_classes((IsAuthenticated,))
	def simpan_tambah_produk(request):
		respon = {'success': False}
		ukuran_list = request.POST.getlist('ukuran[]')
		nama_model_baju = request.POST.get('nama_model_baju', None)
		harga = request.POST.get('harga', None)
		keterangan = request.POST.get('keterangan', None)
		if request.user.penjahit:
			produk_obj = ModelBaju(
				nama_model_baju = nama_model_baju,
				harga = harga,
				penjahit=request.user.penjahit,
				keterangan=keterangan,
				)
			produk_obj.save()

			if ukuran_list:
				_len_ = len(ukuran_list)
				for k in range(_len_):
					if ukuran_list[k] != "":
						produk_obj.ukuran.add(ukuran_list[k])

			if produk_obj:
				penjahit = request.user.penjahit
				# mb = penjahit.modelbaju_set.all().order_by('harga')
				# harga_penjahit = mb.first().harga
				# penjahit.harga = harga_penjahit
				penjahit.save()

			if request.POST.get('foto', None):
				from django.core.files.base import ContentFile
				import base64, uuid, imghdr, os
				try:
					decoded_file = base64.b64decode(request.POST.get('foto', None))
				except TypeError:
					respon = {'success': False, 'pesan': 'Terjadi kesalahan, file yang diupload bukan file gambar'}
				format, imgstr = request.POST.get("foto").split(';base64,') 
				ext = format.split('/')[-1]
				foto = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
				produk_obj.foto = foto
				produk_obj.save()

			respon = {'success': True, 'pesan': 'Data berhasil disimpan'}

		return Response(respon)

	@api_view(['POST'])
	@permission_classes((IsAuthenticated,))
	def simpan_edit_produk(request):
		respon = {'success': False}
		ukuran_list = request.POST.getlist('ukuran[]')
		nama_model_baju = request.POST.get('nama_model_baju', None)
		harga = request.POST.get('harga', None)
		keterangan = request.POST.get('keterangan', None)
		id_produk = request.POST.get('id_produk', None)
		aksi = request.POST.get('aksi', None)
		if request.user.penjahit:
			if id_produk:
				try:
					produk_obj = ModelBaju.objects.get(id=id_produk)
				except ModelBaju.DoesNotExist:
					pass
				else:
					if aksi == "uncheck":
						ukuran_id = request.POST.get('ukuran_id', None)
						produk_obj.ukuran.remove(ukuran_id)
						respon = {'success': True, 'pesan': 'Berhasil Uncheck Ukuran'}
						return Response(respon)

					produk_obj.nama_model_baju = nama_model_baju
					produk_obj.harga = harga
					produk_obj.keterangan = keterangan

					if ukuran_list:
						_len_ = len(ukuran_list)
						for k in range(_len_):
							if ukuran_list[k] != "":
								produk_obj.ukuran.add(ukuran_list[k])

					if request.POST.get('foto', None):
						from django.core.files.base import ContentFile
						import base64, uuid, imghdr, os
						try:
							decoded_file = base64.b64decode(request.POST.get('foto', None))
						except TypeError:
							respon = {'success': False, 'pesan': 'Terjadi kesalahan, file yang diupload bukan file gambar'}
						format, imgstr = request.POST.get("foto").split(';base64,') 
						ext = format.split('/')[-1]
						foto = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
						produk_obj.foto = foto
					produk_obj.save()

					respon = {'success': True, 'pesan': 'Data berhasil disimpan'}

		return Response(respon)

